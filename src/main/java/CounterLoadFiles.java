import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class CounterLoadFiles {
    private static final ConcurrentHashMap<String, Integer> loadedFilesCounts = new ConcurrentHashMap<>();
    private static final Logger logger = Logger.getLogger(Server.class);
    private static final String FILE_COUNTERS = "counters.txt";

    static {
        initLoadedFilesCounts();
    }

    private static void initLoadedFilesCounts() {
        List<String> list = new ArrayList<>(Arrays.asList(new File("files").list()));
        list.forEach(fileName -> loadedFilesCounts.put(fileName, 0));
        logger.info("Load list of file names by directory");
        createCountersFileIfDoesnotExist();
        try (Stream<String> linesStream = Files.lines(Paths.get(FILE_COUNTERS))) {
            List<String> countersList = linesStream.collect(toList());
            countersList.forEach(line -> loadedFilesCounts.replace(line.split(":")[0],
                    Integer.parseInt(line.split(":")[1])));
            logger.info("Load last value of loads of files from file");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void incrementLoadedFileCount(String fileName) {
        loadedFilesCounts.replace(fileName, loadedFilesCounts.get(fileName) + 1);
        logger.info("Increment count of " + fileName);
    }

    public static void writeFileLoadCountsToFile() {
        List<String> lines = new ArrayList<>();
        loadedFilesCounts.forEach((fileName, loadCount) -> lines.add(fileName + ":" + loadCount));
        System.out.println();
        try {
            synchronized(lines) {
                Files.write(Paths.get(FILE_COUNTERS), lines);
                logger.info("write counters of loads of files to file");
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private static void createCountersFileIfDoesnotExist() {
        File file = new File(FILE_COUNTERS);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
            writeFileLoadCountsToFile();
        }
    }

}
