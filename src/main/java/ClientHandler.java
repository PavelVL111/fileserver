import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

public class ClientHandler implements Runnable {
    private Socket clientSocket;
    private static CounterLoadFiles counterLoadFiles = new CounterLoadFiles();
    private static final Logger logger = Logger.getLogger(Server.class);

    public ClientHandler(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void run() {
        try (DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
             DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream())) {
            while (!clientSocket.isClosed()) {
                logger.info("Server reading from socket");
                String message = dataInputStream.readUTF();
                logger.info("Read message from client - " + message);

                if (message.split(":")[0].equals("get file")) {
                    downloadFile(dataOutputStream, message);
                    counterLoadFiles.incrementLoadedFileCount(message.split(":")[1]);
                } else if (message.equals("get list")) {
                    String[] fileNames = new File("files").list();
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(clientSocket.getOutputStream());
                    objectOutputStream.writeObject(fileNames);
                    logger.info("Sent list of files to client");
                } else if (message.equals("stop")) {
                    dataOutputStream.writeUTF("connection broken");
                    clientSocket.close();
                    logger.info("Close socket");
                }
            }
            clientSocket.close();
            logger.info("Closing connections and socket");
        } catch (SocketException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void downloadFile(DataOutputStream dataOutputStream, String message) throws IOException {
        File file = new File("files/" + message.split(":")[1]);
        if (file.exists()) {
            dataOutputStream.writeBoolean(true);
            dataOutputStream.flush();
            logger.info("Sent flag that file is exist");
            byte[] bytes = new byte[(int) file.length()];
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            bis.read(bytes, 0, bytes.length);
            dataOutputStream.writeLong(file.length());
            logger.info("Sent length of file");
            OutputStream outputStream = clientSocket.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            logger.info("Sent file to client");
        } else {
            dataOutputStream.writeBoolean(false);
            dataOutputStream.flush();
            logger.info("Sent flag that file doesn't exist");
        }
    }

}

