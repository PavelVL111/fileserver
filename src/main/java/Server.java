import org.apache.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Server {
    static ExecutorService executorService = Executors.newCachedThreadPool();
    private static final Logger logger = Logger.getLogger(Server.class);
    private static final int PORT_SERVER = 3300;
    private static final int COUNT_FILES_IN_FOLDER = 5;
    private static final int SECENDS_SLEEP_THREAD = 5;

    public static void main(String[] args) {
        logger.info("Start server");
        startCountWriterThread();
        startCloseListener();
        createFilesDirIfDoesNotExist();
        try (ServerSocket serverSocket = new ServerSocket(PORT_SERVER)) {

            logger.info("Server socket created, command console reader for listen to server commands");

            while (!serverSocket.isClosed()) {
                logger.info("Waite new connection");
                Socket clientSocket = serverSocket.accept();
                DataInputStream dataInputStream = new DataInputStream(clientSocket.getInputStream());
                if (dataInputStream.readUTF().equals("stop")){
                    logger.info("Accepted message 'stop'");
                    dataInputStream.close();
                    break;
                } else {
                    executorService.execute(new ClientHandler(clientSocket));
                }
            }
            executorService.shutdownNow();
            logger.info("Thread for interaction with client is shutdown");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void createFilesDirIfDoesNotExist() {
        File folder = new File("files");
        if (!folder.exists()) {
            folder.mkdir();
        }
        createFiles();
    }

    public static void createFiles() {
        try {
            for (int i = 1; i <= COUNT_FILES_IN_FOLDER; i++) {
                File file = new File("files/file" + i + ".txt");
                FileWriter fileWriter = new FileWriter(file);
                fileWriter.write("some text " + i);
                fileWriter.flush();
                fileWriter.close();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    public static void startCountWriterThread() {
        logger.info("Start pushing counts thread");
        Thread thread = new Thread(() -> {
            try {
                while (!executorService.isShutdown()) {
                    TimeUnit.SECONDS.sleep(SECENDS_SLEEP_THREAD);
                    CounterLoadFiles.writeFileLoadCountsToFile();
                    logger.info("Push counters");
                }
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
        });
        thread.start();
    }

    public static void startCloseListener() {
        logger.info("Start listener of thread for console");
        Thread thread = new Thread(() -> {
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
                while (true) {
                    if (bufferedReader.readLine().equals("stop")) {
                        logger.info("Read massage 'stop' from console");
                        interruptAcceptClients();
                        break;
                    }
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
                interruptAcceptClients();
            }
        });
        thread.start();
    }

    public static void interruptAcceptClients() {
        try {
            Socket socket = new Socket("localhost", PORT_SERVER);
            logger.info("Create socked for sent stop massage");
            DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataOutputStream.writeUTF("stop");
            logger.info("Sent stop massage");
            dataOutputStream.flush();
            socket.getOutputStream().flush();
            dataOutputStream.close();
            socket.close();
            logger.info("Close socket");
        } catch (IOException e) {
        }
    }

}
